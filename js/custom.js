jQuery(document).ready(function(){
	//testimonial slider
	$('.owl-carousel').owlCarousel({
	    loop: false,
	    margin: 0,
	    nav: true,
	    dots: false,
	    items: 1
	});

	//tab
	$( "#tabs" ).tabs();

	//model
	$('.btn-signup').click(function(){
		$('body').addClass('signup-model-open model-open');
		$('.btn-close-model').click(function(){
			$('body').removeClass('signup-model-open model-open');
		});
	});

	$('.btn-login').click(function(){
		$('body').addClass('login-model-open model-open');
		$('.btn-close-model').click(function(){
			$('body').removeClass('login-model-open model-open');
		});
	});

	$('.has-account .btn-login').click(function(){
		$('body').removeClass('signup-model-open');
	});

	$('.goto-signup .btn-signup').click(function(){
		$('body').removeClass('login-model-open');
	});

	$('.forgot-password').click(function(){
		$('body').addClass('password-recovery-model-open model-open');
		$('body').removeClass('login-model-open');
		$('.btn-close-model').click(function(){
			$('body').removeClass('password-recovery-model-open model-open');
		});
	});

	$('.btn-to-checkout').click(function(){
		$('body').addClass('checkout-model-open model-open');
		$('.btn-close-model').click(function(){
			$('body').removeClass('checkout-model-open model-open');
		});
	});

	//menu animation
	window.slide = new SlideNav();
	var nav = new SlideNav({
	    activeClass: "active",
	    toggleButtonSelector: false,
	    toggleBoxSelector: false,
	    hideAfterSelect: true,
	    speed: 70,
	    changeHash: false,
	    navBoxToggleClass: false
	});

	//window scroll
    $(window).scroll(function() {
        if ($(window).scrollTop() >= 1) {
            $(".site-header").addClass("scrolled-header");
        } else {
            $(".site-header").removeClass("scrolled-header");
        }
    });

    //responsive menu
    $('.btn-menu').click(function(){
    	$('body').toggleClass('menu-open');
    	$('.overlay').click(function(){
    		$('body').removeClass('menu-open');
    	});

    	$('.main-navigation ul li a').click(function(){
    		$('body').removeClass('menu-open');
    	});
    });

    //logout dropdown
    $('.login-tool').click(function(){
    	$('.btn-logout').toggleClass('show');
    	$('body').click(function(){
    		$('.btn-logout').removeClass('show');
    	});
    	event.stopPropagation();
    });

    //datepicker
    $( "#datepicker-from" ).datepicker();
    $( "#datepicker-to" ).datepicker();

    //close note in create page
    $('.btn-close-note').click(function(){
    	$('.create-order .note').hide();
    });

    //table option show-hide
    $('.table .options .dots').click(function(){
    	$(this).siblings().toggle();
    	$('body').click(function(){
    		$('.table .options .option-drop').hide();

    	});
    	event.stopPropagation();
    });

    $('.table .btn-detail').click(function(){
    	$(this).toggleClass('active');
    	$(this).parent().parent().parent().toggleClass('detail-open');
    	$('.inner-table-holder .close-table').click(function(){
    		$(this).parent().parent().parent().removeClass('detail-open');
    	});
    });

})